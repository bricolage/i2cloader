/* Copyright(C) 2005,2006,2007,2008 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
//#define F_CPU 8000000UL  // 8 MHz
#include <util/delay.h>
#include <avr/boot.h>
#include <inttypes.h>

#ifdef TEENSY

void __attribute__ ((naked, section(".init3"))) init_avr (void);

void 
init_avr (void)
{
  /* clear zero-register */
  __asm volatile ("eor r1, r1 \n\t");

  /* clear sreg */
  __asm volatile ("out __SREG__, r1 \n\t");

  /* initialize stack pointer */
  __asm volatile ("ldi r28, lo8(%0) \n\t"
		  "ldi r29, hi8(%0) \n\t"
		  "out __SP_H__, r29     \n\t"
		  "out __SP_L__, r28     \n\t"
		  :
		  : "i" (RAMEND));
}

#define naked    __attribute__((naked))

#ifdef MITLED
#undef MITLED
#define MITLED 0
#endif

#endif

/* MITLED definiert ob LED bei zugriffen blinken soll. 
 * Ohne LED ist der Code unter 512 bytes gross!
 */
#ifndef LEDPIN
#define LEDDDR DDRB
#define LEDPORT PORTB
#define LEDPIN PB0
#endif

#define STATUSLED(s) (LEDPORT = (LEDPORT &~_BV(LEDPIN)) | ((s) ? _BV(LEDPIN) : 0))

static void boot_program_page (uint16_t page, uint8_t *buf);

#if MITLED
void
blink(uint8_t blinki, uint8_t puls, uint8_t pause)
{
  uint8_t pulstemp = puls;
  uint8_t pausetemp = pause;
  while(blinki > 0){
    pulstemp = puls;
    pausetemp = pause;
    STATUSLED(1);//PORTB |= _BV(PB0);
    while(pulstemp > 0){
     _delay_ms(25);
      pulstemp--;
    }
    STATUSLED(0);//PORTB &= ~_BV(PB0);
    while(pausetemp > 0){
      _delay_ms(25);
      pausetemp--;
    }
    blinki--;
  }
}
#endif

#ifdef TEENSY
naked int main(void)
#else
int main(void)
#endif
{
  
  uint8_t byteanzahl;
  uint8_t smbuscommand;
  uint8_t smbuscount;
  uint8_t Checksum;
  uint16_t pageaddr;
  uint8_t bufaddr;
#ifndef TEENSY
  byteanzahl = 0;
  smbuscommand = 0;
  smbuscount = 0;
  Checksum = 0;
  pageaddr = 0;
  bufaddr = 0;
#endif
  uint8_t buf[64];
  uint16_t timeout = 0;
  
  void (*applptr)( void ) = 0x0000;
  cli();
#if MITLED
  LEDDDR |= _BV(LEDPIN);  //LED pin auf ausgang
  blink(2, 20, 20);
#endif
 /* hier wird die Addresse des µC festgelegt
  * (in den oberen 7 Bit, das LSB(niederwertigstes Bit)
  * steht dafür ob der µC auf einen general callreagiert
 */
  TWAR = 0x08;
 /* TWI Control Register, hier wird der TWI aktiviert,
  *der Interrupt aktiviert und solche Sachen
 */
  TWCR = (1<<TWEN) | (1<<TWEA) | (1<<TWINT);


  while (1) { /* Endlosschleife weil 1 immer WAHR*/
    
    if((TWCR & _BV(TWINT)) != 0){
      if((TWSR & 0xF8) == 0x80){
  /* Datenbyte wurde empfangen hier Code einfügen,
	* der bearbeitet werden soll, die empfangenen
	* Daten kann man aus TWDR auslesen
  */
	if (byteanzahl == 0){
	  smbuscommand = TWDR;
	}
	if (byteanzahl == 1){
	  smbuscount = TWDR;
	}

	/* empfangen der flash schreib adresse */
	if (smbuscommand == 0xF1){
	  if (byteanzahl == 0){
	    bufaddr = 0;
	    Checksum = 0;
	  }
	  else if (byteanzahl == 2){
	    pageaddr = TWDR;
	  }
	  else if (byteanzahl == 3){
	    pageaddr |= TWDR<<8;
	  }
	}
	/* empfangen der datenbloecke */
	if (smbuscommand == 0xF2 && byteanzahl > 1 && smbuscount > 0){
	  buf[bufaddr++] = TWDR;
	  Checksum = (Checksum + TWDR) & 0xFF;
	  smbuscount--;
	  if(bufaddr == 64){
#if MITLED
	    STATUSLED(1);//PORTB |= _BV(PB0);
#endif
	    boot_program_page (pageaddr, buf);
#if MITLED
	    STATUSLED(0);//PORTB &= ~_BV(PB0);
#endif
    
	  }
	}

	/* bootloader quit und start der app */
	if (smbuscommand == 0xFE && smbuscount == 1 && byteanzahl == 2 && TWDR == 0xFF){
#if MITLED
	  blink(5, 5, 10);
#endif
	  applptr(); // Rücksprung zur Application
	}

	byteanzahl++;
      }
      else if((TWSR & 0xF8) == 0x60){
	/* Der Avr wurde mit seiner Adresse angesprochen  */
	timeout = 0xFFFF;
	byteanzahl = 0;
      }

      /* hier wird an den Master gesendet */
      else if((TWSR & 0xF8) == 0xA8){
	if(smbuscommand == 0xF3){
	  TWDR = 1;
	}
	else if(smbuscommand == 0xF0){
#if MITLED
	  STATUSLED(1);//PORTB |= _BV(PB0);
#endif
	  TWDR = 1;
	}
	else if(smbuscommand == 0xF4){
#if MITLED
	  STATUSLED(1);//PORTB |= _BV(PB0);
#endif
	  TWDR = 0x10;
	  smbuscount = 0x00;
	}
	else{
	  TWDR = 0;
	}
	byteanzahl++;
      }
      else if((TWSR & 0xF8) == 0xB8){
	if(smbuscommand == 0xF3){
	  TWDR = Checksum;
	}
	else if(smbuscommand == 0xF0){
	  TWDR = 0xFF;
	}
	else if(smbuscommand == 0xF4){
	  if(smbuscount < 0x10){
	    TWDR = pgm_read_byte(pageaddr++);
	    smbuscount++;
	  }

#if MITLED
	  else STATUSLED(0);//PORTB &= ~_BV(PB0);
#endif
	}
	else{
	  TWDR = 0;
	}
	byteanzahl++;
      }

      /* wenn der Interrupt ausgelöst wird, wird der TWI des µC blockiert,
      * damit man die Daten verarbeiten kann
      * um ihn wieder zu aktivieren, muss man eben folgenden Befehl ausführe
      */
      TWCR = (1<<TWEN) | (1<<TWEA) | (1<<TWINT);
    }

    if(TWSR == 0){
      TWCR = 0;
      TWCR = (1<<TWEN) | (1<<TWEA) | (1<<TWINT);
    }

    /* Bootloader Quit und sprung zur App */
    if(timeout == 0xFFFE){
#if MITLED
      blink(5, 5, 10);
#endif
      applptr(); // Ruecksprung zur Application
    }
    /* timeout wenn der bootloader nicht angesprochen wird */
    if(timeout != 0xFFFF){
      timeout++;
      _delay_us(50);
    }
  }
}

static void
boot_program_page (uint16_t page, uint8_t *buf)
{
  uint16_t i;

  eeprom_busy_wait ();

  boot_page_erase (page);
  boot_spm_busy_wait ();      // Wait until the memory is erased.
  uint8_t wbufaddr = 0;

  for (i=0; i<SPM_PAGESIZE; i+=2)
  {
    // Set up little-endian word.
    uint16_t w = buf[wbufaddr++];
    w += buf[wbufaddr++] << 8;

    boot_page_fill (page + i, w);
  }

  boot_page_write (page);     // Store buffer in flash page.
  boot_spm_busy_wait();       // Wait until the memory is written.

  // Reenable RWW-section again. We need this if we want to jump back
  // to the application after bootloading.

  boot_rww_enable ();

}

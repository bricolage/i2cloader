PROGRAMMER=stk200
MCU=atmega8
CC=avr-gcc
OBJCOPY=avr-objcopy
F_CPU=8000000UL
CFLAGS=-g -mmcu=$(MCU) -DF_CPU=$(F_CPU) -Wall -Wstrict-prototypes -Os -W

#- default LED-PIN
LED=PB0

#- ... mangle things a bit.
PORT=`echo ${LED} | cut -c2`
PIN=`echo ${LED} | cut -c3`
LEDPIN=-DLEDDDR=DDR${PORT} -DLEDPORT=PORT${PORT} -DLEDPIN=P${PORT}${PIN}

all: i2c-bootloader-512w-led.hex i2c-bootloader-512w.hex i2c-bootloader-256w.hex i2c-test i2cloader

i2c-bootloader-512w-led.hex i2c-bootloader-512w.hex i2c-bootloader-256w.hex: %.hex: %
	$(OBJCOPY) -R .eeprom -O ihex $< $@

i2c-bootloader-512w-led: i2c-bootloader-led.o
	$(CC) $(CFLAGS) -o $@ -Wl,--section-start=.text=0x1C00 \
		-Wl,-Map,i2c-bootloader-512w.map $<

i2c-bootloader-512w: i2c-bootloader.o
	$(CC) $(CFLAGS) -o $@ -Wl,--section-start=.text=0x1C00 \
		-Wl,-Map,i2c-bootloader-512w.map $<

i2c-bootloader-256w: i2c-bootloader-teensy.o
	$(CC) $(CFLAGS) -nostdlib -o $@ -Wl,--section-start=.text=0x1E00 \
		-Wl,-Map,i2c-bootloader-256w.map $<

i2c-bootloader-led.o: i2c-bootloader.c
	$(CC) -o $@ -c $< -DMITLED $(CFLAGS) $(LEDPIN)

i2c-bootloader-teensy.o: i2c-bootloader.c
	$(CC) -o $@ -c $< -DTEENSY $(CFLAGS) -Wno-uninitialized

i2c-bootloader.s: i2c-bootloader.c
	$(CC) $(CFLAGS) -S -DMITLED $<

i2c-test: i2c-test.c i2c-host-routines.c i2c-host-routines.h
	gcc -Wall -ggdb -o i2c-test i2c-host-routines.c i2c-test.c

i2cloader: i2cloader.c i2c-host-routines.c i2c-host-routines.h 
	gcc -Wall -ggdb -o i2cloader i2cloader.c -lsmbus

clean:
	rm -f *.map *.hex i2c-bootloader-*w *.o *.s *~ i2c-test i2cloader

load-512w-led: i2c-bootloader-512w-led.hex
	avrdude -p $(MCU) -U flash:w:$< -E vcc,noreset

load-512w: i2c-bootloader-512w.hex
	avrdude -p $(MCU) -U flash:w:$< -E vcc,noreset

load-256w: i2c-bootloader-256w.hex
	avrdude -p $(MCU) -U flash:w:$< -E vcc,noreset

# the default of the atmega8 hfuse (D9)
fuse-512w: #resetvector auf application und bootloaderspeicher auf 0E00
	avrdude -p m8 -U hfuse:w:0xDB:m -E vcc,noreset

fuse-256w: #resetvector auf application und bootloaderspeicher auf 0F00
	avrdude -p m8 -U hfuse:w:0xDD:m -E vcc,noreset

fuse-reset-512w: #resetvector auf bootloader und bootloaderspeicher auf 0E00
	avrdude -p m8 -U hfuse:w:0xDA:m -E vcc,noreset

fuse-reset-256w: #resetvector auf bootloader und bootloaderspeicher auf 0E00
	avrdude -p m8 -U hfuse:w:0xDC:m -E vcc,noreset

fuse-BOD-8Mhz: #speedup to 8Mhz and enable BOD
	avrdude -p m8 -U lfuse:w:0xA4:m -E vcc,noreset

loaduisp-512w-led: i2c-bootloader-512w-led.hex
	uisp --erase  -dprog=$(PROGRAMMER)
	uisp --upload if=$< -dprog=$(PROGRAMMER)  -v=3 --hash=32

loaduisp-512w: i2c-bootloader-512w.hex
	uisp --erase  -dprog=$(PROGRAMMER)
	uisp --upload if=$< -dprog=$(PROGRAMMER)  -v=3 --hash=32

loaduisp-256w: i2c-bootloader-256w.hex
	uisp --erase  -dprog=$(PROGRAMMER)
	uisp --upload if=$< -dprog=$(PROGRAMMER)  -v=3 --hash=32

loadadd-512w-led: i2c-bootloader-512w-led.hex
	avrdude -p $(MCU) -D -U flash:w:$< -E vcc,noreset

loadadd-512w: i2c-bootloader-512w.hex
	avrdude -p $(MCU) -D -U flash:w:$< -E vcc,noreset

loadadd-256w: i2c-bootloader-256w.hex
	avrdude -p $(MCU) -D -U flash:w:$< -E vcc,noreset

install: i2cloader
	install i2cloader /usr/local/bin/
